//
//  GameScene.swift
//  SaveToArk
//
//  Created by alexbutenko on 12/20/16.
//  Copyright © 2016 alexbutenko. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    override func didMove(to view: SKView) {
        let dial = Dial()
        dial.generateSector()
        let grid = Grid(blockSize: 64.0,
                        rows: 26,
                        cols: 20,
                        dial: dial,
                        shipPosition: GridPoint(row:11, col:0))
        
        let gridNode = GridNode(grid:grid, view:view, scene:self)
        gridNode.position = CGPoint.zero
        gridNode.zPosition = 100
        addChild(gridNode)
        
        //add few animals for example
        let animal1 = AnimalNode(id: 6)
        animal1.position = grid.gridPosition(gridPoint: GridPoint(row:7, col:13))
        gridNode.addChild(animal1)
        
        let animal2 = AnimalNode(id: 7)
        animal2.position = grid.gridPosition(gridPoint: GridPoint(row:4, col:12))
        gridNode.addChild(animal2)
        
        let animal3 = AnimalNode(id: 12)
        animal3.position = grid.gridPosition(gridPoint: GridPoint(row:11, col:13))
        gridNode.addChild(animal3)
        
        //add ship for example
        let ship = ShipNode()
        ship.position = grid.gridPosition(gridPoint: grid.shipPosition)
        gridNode.addChild(ship)
        
        let dialNode = DialNode(parent:self, zValue:gridNode.zPosition + 1, dial:dial)
        addChild(dialNode)
        dialNode.rotate()

        let goToNextMove:(GridPoint)->Void = {gridPoint in
            grid.shipPosition = gridPoint
            ship.position = grid.gridPosition(gridPoint: grid.shipPosition)
            
            if let animalNode = gridNode.animalNode(at: ship.position) {
                animalNode.removeFromParent()
                ship.animalsOnBoardNodes.append(animalNode)
            }
            
            gridNode.displayCaughtAnimals()

            dial.generateSector()
            gridNode.displayAvailableMoves()
            dialNode.rotate()
        }
        
        gridNode.onTap = { gridPoint in
            if grid.isLegalPointToMove(point: gridPoint) {
                goToNextMove(gridPoint)
            }
        }
        
        dial.onStorm = {
            if let gridInterval = grid.availableMovesIntervals().first {
                goToNextMove(gridInterval.endPoint)
            }
        }
        
        gridNode.displayAvailableMoves()
    }
}
