
//  Extensions.swift
//  SaveToArk
//
//  Created by Alex Butenko on 17/11/17.
//  Copyright © 2017 alexbutenko. All rights reserved.
//

import SpriteKit

struct PixelData {
    var a: UInt8 = 0
    var r: UInt8 = 0
    var g: UInt8 = 0
    var b: UInt8 = 0
}

extension PixelData {
    init(rgba:[UInt8]) {
        self.init(a: rgba[3], r: rgba[0], g: rgba[1], b: rgba[2])
    }
}

extension PixelData : Equatable {
    static func ==(lhs: PixelData, rhs: PixelData) -> Bool {
        return lhs.a == rhs.a && lhs.r == rhs.r && lhs.g == rhs.g && lhs.b == rhs.b
    }
}

extension UIImage {
    
    func rgbComponents(at point:CGPoint) -> PixelData {
        var pixel: [UInt8] = [0, 0, 0, 0]
        
        guard let image = cgImage else {
            return PixelData(rgba: pixel)
        }
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        
        let context = CGContext(data: &pixel,
                                width: 1,
                                height: 1,
                                bitsPerComponent: 8,
                                bytesPerRow: 4,
                                space: colorSpace,
                                bitmapInfo: bitmapInfo.rawValue)!
        
        context.setBlendMode(.copy)
        context.scaleBy(x: 1, y: -1)
        context.translateBy(x: -point.x, y: point.y - CGFloat(image.height))
        
        context.draw(image, in: CGRect(x: 0, y: 0, width: image.width, height: image.height))
        
        return PixelData(rgba: pixel)
    }
    
    static func imageFromBitmap(pixels: [PixelData], width: Int, height: Int) -> UIImage? {
        let pixelDataSize = MemoryLayout<PixelData>.size
        guard width > 0, height > 0, pixelDataSize == 4, pixels.count == Int(width * height) else {
            return nil
        }

        let data: Data = pixels.withUnsafeBufferPointer {
            return Data(buffer: $0)
        }

        let cfdata = NSData(data: data) as CFData
        let provider: CGDataProvider! = CGDataProvider(data: cfdata)
        if provider == nil {
            print("CGDataProvider is not supposed to be nil")
            return nil
        }
        let cgimage: CGImage! = CGImage(
            width: width,
            height: height,
            bitsPerComponent: 8,
            bitsPerPixel: 32,
            bytesPerRow: width * pixelDataSize,
            space: CGColorSpaceCreateDeviceRGB(),
            bitmapInfo: CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedFirst.rawValue),
            provider: provider,
            decode: nil,
            shouldInterpolate: true,
            intent: .defaultIntent
        )
        if cgimage == nil {
            print("CGImage is not supposed to be nil")
            return nil
        }
        return UIImage(cgImage: cgimage)
    }
}

extension UIView {
    func getScreenshot() -> UIImage {
        //we need to disregard screen scale for simpler pixel data retrieving
        UIGraphicsBeginImageContextWithOptions(UIScreen.main.bounds.size, false, 1.0)
        drawHierarchy(in: bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
