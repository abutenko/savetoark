//
//  AnimalNode.swift
//  SaveToArk
//
//  Created by Alex Butenko on 5/01/18.
//  Copyright © 2018 alexbutenko. All rights reserved.
//

import SpriteKit

class AnimalNode : SKSpriteNode {
    var id: Int!
    
    convenience init(id: Int) {
        self.init(imageNamed: "animal_\(id)")
        
        self.id = id
        self.name = "animal"
    }
}
