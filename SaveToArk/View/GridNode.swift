//
//  GridNode.swift
//  SaveToArk
//
//  Created by alexbutenko on 12/20/16.
//  Copyright © 2016 alexbutenko. All rights reserved.
//

import SpriteKit
import QuartzCore
import NVActivityIndicatorView

class GridNode:SKSpriteNode {
    var onTap: ((GridPoint)->Void)?
    var grid:Grid!
    var view:SKView!
    var parentScene:SKScene!
    var screenshot:UIImage!
    
    convenience init(grid:Grid, view:SKView, scene:SKScene) {
        let texture = GridNode.gridTexture(blockSize:grid.blockSize, rows:grid.rows, cols:grid.cols)
        self.init(texture:texture, color:SKColor.clear, size:texture.size())
        self.grid = grid
        self.parentScene = scene
        self.view = view
        isUserInteractionEnabled = true
        
        screenshot = view.getScreenshot()
        
        let indicatorFrame = CGRect(origin: view.center,
                                    size: CGSize(width: 100.0, height: 100.0))
        let activityIndicator = NVActivityIndicatorView(frame: indicatorFrame,
                                                        type: .pacman,
                                                        color: .black)
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        view.isUserInteractionEnabled = false

        grid.loadLandPointsIfNeeded(with: screenshot,
                                    map: { [weak self] in
                                        guard let weakSelf = self else {
                                            return .zero
                                        }
            
                                        return weakSelf.view.convert($0, from: weakSelf.parentScene)
            },
                                    completion: {
                                        view.isUserInteractionEnabled = true
                                        activityIndicator.stopAnimating()
                                        activityIndicator.removeFromSuperview()
        })
    }
    
    override init(texture:SKTexture!, color:SKColor, size:CGSize) {
        super.init(texture:texture, color:color, size:size)
    }
    
    required init?(coder aDecoder:NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class func gridTexture(blockSize:CGFloat, rows:Int, cols:Int) -> SKTexture {
        // Add 1 to the height and width to ensure the borders are within the sprite
        let size = CGSize(width: CGFloat(cols)*blockSize+1.0, height: CGFloat(rows)*blockSize+1.0)
        UIGraphicsBeginImageContext(size)
        
        let context = UIGraphicsGetCurrentContext()
        let bezierPath = UIBezierPath()
        let offset:CGFloat = 0.5
        // Draw vertical lines
        for i in 0...cols {
            let x = CGFloat(i)*blockSize + offset
            bezierPath.move(to: CGPoint(x: x, y: 0))
            bezierPath.addLine(to: CGPoint(x: x, y: size.height))
        }
        // Draw horizontal lines
        for i in 0...rows {
            let y = CGFloat(i)*blockSize + offset
            bezierPath.move(to: CGPoint(x: 0, y: y))
            bezierPath.addLine(to: CGPoint(x: size.width, y: y))
        }
        SKColor.black.setStroke()
        bezierPath.lineWidth = 1.0
        bezierPath.stroke()
        context!.addPath(bezierPath.cgPath)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return SKTexture(image: image!)
    }
        
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            var location = touch.location(in: self)
            //location from top left
            location.x += frame.width/2
            location.y -= frame.height/2
            location.y = abs(location.y)
            
            let gridPoint = grid.gridPoint(location: location)
            onTap?(gridPoint)
        }
    }
    
    func displayAvailableMoves() {
        enumerateChildNodes(withName: "tintedSquare") { node, stop in
            node.removeFromParent()
        }
        
        let intervals = grid.availableMovesIntervals()
        
        for interval in intervals {
            for point in interval {
                if !grid.isLandNode(at: point) {
                    let tintedSquare = SKSpriteNode(color: UIColor.gray.withAlphaComponent(0.5),
                                                    size: CGSize(width:grid.blockSize,
                                                                 height:grid.blockSize))
                    tintedSquare.position = grid.gridPosition(gridPoint: point)
                    tintedSquare.name = "tintedSquare"
                    addChild(tintedSquare)
                }
            }
        }
    }
    
    func displayLandPoints() {
        enumerateChildNodes(withName: "landSquare") { node, stop in
            node.removeFromParent()
        }
        
        for point in grid.landPoints {
            let landSquare = SKSpriteNode(color: UIColor.red.withAlphaComponent(0.5),
                                            size: CGSize(width:grid.blockSize,
                                                         height:grid.blockSize))
            landSquare.position = grid.gridPosition(gridPoint: point)
            landSquare.name = "landSquare"
            addChild(landSquare)
        }
    }
    
    func animalNode(at point:CGPoint) -> AnimalNode? {
        var result:AnimalNode? = nil
        enumerateChildNodes(withName: "animal") { node, stop in
            if node.position == point {
                result = node as? AnimalNode
                stop.initialize(to: true)
            }
        }
        
        return result
    }
    
    func displayCaughtAnimals() {
        if let shipNode = childNode(withName: "ship") as? ShipNode {
            for index in 0..<shipNode.animalsOnBoardNodes.count {
                let animalNode = shipNode.animalsOnBoardNodes[index]
                
                if animalNode.parent == nil {
                    let point = GridPoint(row: 0, col: grid.cols - shipNode.animalCapacity + index)
                    animalNode.position = grid.gridPosition(gridPoint: point)
                    addChild(animalNode)
                }
            }
        }
    }
}

