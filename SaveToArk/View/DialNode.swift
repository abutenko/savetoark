//
//  DialNode
//  SaveToArk
//
//  Created by Alex Butenko on 10/11/17.
//  Copyright © 2017 alexbutenko. All rights reserved.
//

import SpriteKit

class DialNode : SKSpriteNode {
    var dial:Dial!
    
    convenience init(parent:SKNode, zValue:CGFloat, dial:Dial) {
        self.init(imageNamed:"dial")
        
        position = CGPoint(x: frame.width - parent.frame.width/2 - 50,
                           y: parent.frame.height/2 - frame.height/2 - 20)
        zPosition = zValue
        
        let length: CGFloat = 75
        
        var points = [CGPoint(x:length / 2.0, y:-length / 2.0),
                      CGPoint(x:-length / 2.0, y:-length / 2.0),
                      CGPoint(x: 0.0, y: length),
                      CGPoint(x:length / 2.0, y:-length / 2.0)]
        
        let triangle = SKShapeNode(points: &points, count: points.count)
        
        triangle.fillColor = UIColor.black
        
        triangle.position = CGPoint(x:position.x, y:position.y + 15)
        triangle.zPosition = zPosition + 1
        
        parent.addChild(triangle)
        
        self.dial = dial
    }
    
    func rotate() {
//        var additionalSpins = Double(1 + arc4random_uniform(UInt32(3))) * 2
//        print("additionalSpins \(additionalSpins)")
//        additionalSpins*=Double.pi
        
//        let randomRotations = SKAction.rotate(byAngle: CGFloat(additionalSpins), duration: 2)
        let rotateToAngle = SKAction.rotate(toAngle: dial.angle, duration: 2)
//        let sequence = SKAction.sequence([randomRotations, rotateToAngle])
//        run(sequence)
        run(rotateToAngle)
    }
}
