//
//  ShipNode.swift
//  SaveToArk
//
//  Created by Alex Butenko on 5/01/18.
//  Copyright © 2018 alexbutenko. All rights reserved.
//

import SpriteKit

class ShipNode : SKSpriteNode {
    var animalsOnBoardNodes: [SKSpriteNode] = []
    let animalCapacity = 4
    
    convenience init() {
        self.init(imageNamed: "ship")
        self.name = "ship"
    }
}
