//
//  GridInterval.swift
//  SaveToArk
//
//  Created by alexbutenko on 7/8/17.
//  Copyright © 2017 alexbutenko. All rights reserved.
//

import Foundation

struct GridPoint: Codable {
    var row: Int
    var col: Int
}

extension GridPoint: Equatable {
    static func ==(lhs: GridPoint, rhs: GridPoint) -> Bool {
        return lhs.row == rhs.row && lhs.col == rhs.col
    }
}

struct GridInterval {
    var startPoint: GridPoint
    var endPoint: GridPoint
    
    init(startPoint: GridPoint, endPoint: GridPoint,
         isLegalNodeToMoveCheck: ((GridPoint) -> Bool)? = nil) {
        self.startPoint = startPoint
        self.endPoint = endPoint
        
        var prevPoint = startPoint
        
        for point in self {
            if isLegalNodeToMoveCheck?(point) == false {
                self.endPoint = prevPoint
                break
            }
            
            prevPoint = point
        }
    }
}

extension GridInterval: Equatable {
    static func ==(lhs: GridInterval, rhs: GridInterval) -> Bool {
        return lhs.startPoint == rhs.startPoint && lhs.endPoint == rhs.endPoint
    }
}

extension GridInterval : Sequence {
    func makeIterator() -> GridIntervalIterator {
        return GridIntervalIterator(interval: self)
    }
}

class GridIntervalIterator : IteratorProtocol {
    var i = 0, j = 0
    var currentPoint:GridPoint?
    var interval:GridInterval!
    
    init(interval:GridInterval){
        self.interval = interval
    }
    
    func next() -> GridPoint? {
        if currentPoint == interval.endPoint {
            return nil
        }
        
        currentPoint = GridPoint(row: interval.startPoint.row + i, col: interval.startPoint.col + j)
        
        i = interval.endPoint.row > interval.startPoint.row ? min(i + 1, interval.endPoint.row - interval.startPoint.row) :
            max(i - 1, interval.endPoint.row - interval.startPoint.row)
        j = interval.endPoint.col > interval.startPoint.col ? min(j + 1, interval.endPoint.col - interval.startPoint.col) :
            max(j - 1, interval.endPoint.col - interval.startPoint.col)
        
        return currentPoint
    }
}

extension Grid : Sequence {
    func makeIterator() -> GridIterator {
        return GridIterator(grid: self)
    }
}

class GridIterator : IteratorProtocol {
    var i = 0, j = 0
    var currentPoint:GridPoint?
    var grid:Grid!

    var lastPoint:GridPoint {
        get {
            return GridPoint(row: grid.rows - 1, col: grid.cols - 1)
        }
    }
    
    init(grid:Grid){
        self.grid = grid
    }
    
    func next() -> GridPoint? {
        if currentPoint == lastPoint {
            return nil
        }
        
        currentPoint = GridPoint(row: i, col: j)
        
        if (j < grid.cols - 1) {
            j += 1
        } else {
            j = 0
            i = Swift.min(i + 1, grid.rows - 1)
        }
        
        return currentPoint
    }
}
