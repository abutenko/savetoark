//
//  Grid.swift
//  SaveToArk
//
//  Created by alexbutenko on 1/28/17.
//  Copyright © 2017 alexbutenko. All rights reserved.
//

import Foundation
import CoreGraphics
import UIKit.UIImage

class Grid {
    var rows:Int!
    var cols:Int!
    var blockSize:CGFloat!
    var dial:Dial!
    var shipPosition:GridPoint!
    var landPoints:[GridPoint] = []
    var landingPointsURL:URL {
        let documentsPathURL = FileManager.default.urls(for: .documentDirectory,
                                                        in: .userDomainMask).first!
        return documentsPathURL.appendingPathComponent("landing_points.plist")
    }
    
    init(blockSize:CGFloat, rows:Int, cols:Int, dial:Dial, shipPosition:GridPoint) {
        self.blockSize = blockSize
        self.rows = rows
        self.cols = cols
        self.dial = dial
        self.shipPosition = shipPosition
    }
    
    func gridPosition(gridPoint:GridPoint) -> CGPoint {
        let offset = blockSize / 2.0 + 0.5
        let x = CGFloat(gridPoint.col) * blockSize - (blockSize * CGFloat(cols)) / 2.0 + offset
        let y = CGFloat(rows - gridPoint.row - 1) * blockSize - (blockSize * CGFloat(rows)) / 2.0 + offset
        return CGPoint(x:x, y:y)
    }
    
    func gridRect(gridPoint:GridPoint) -> CGRect {
        //cause of offset is unclear
        let offset:CGFloat = 8
        let sizeWithOffset = (blockSize/2 ) + offset
        
        var gridPosition = self.gridPosition(gridPoint: gridPoint)
        gridPosition.x -= sizeWithOffset/2.0
        gridPosition.y += sizeWithOffset/2.0
        
        gridPosition.x -= offset + 4
        gridPosition.y += offset + 4

        let gridRect = CGRect(origin: gridPosition, size: CGSize(width: sizeWithOffset, height: sizeWithOffset))
        return gridRect
    }
    
    func gridPoint(location:CGPoint) -> GridPoint {
        let row = Int(location.y/blockSize)
        let col = Int(location.x/blockSize)
        return GridPoint(row: row, col: col)
    }
    
    func availableMovesIntervals() -> [GridInterval] {
        var intervals:[GridInterval] = []
        
        switch dial.windStrength {
        case .noWind:
            //you can move one square in any direction
            for direction in Grid.allDirections {
                let endPoint = shiftedPointWith(direction: direction, shift: 1)
                intervals += [GridInterval(startPoint:shipPosition,
                                           endPoint: endPoint,
                                           isLegalNodeToMoveCheck: isaAvailableNodeToMove)]
            }
        case .normal:
            //you can move 0-6 squares depending on direction
            for direction in Grid.allDirections {
                let endPoint = shiftedPointWith(direction: direction,
                                                shift: normalWindShiftValueFor(direction: direction))
                intervals += [GridInterval(startPoint:shipPosition,
                                           endPoint: endPoint,
                                           isLegalNodeToMoveCheck: isaAvailableNodeToMove)]
            }
        case .storm:
            //you get carried the way the storm is headed automatically.
            //TODO: find out shift value
            let endPoint = shiftedPointWith(direction: dial.windDirection, shift: 2)
            intervals += [GridInterval(startPoint:shipPosition,
                                       endPoint: endPoint,
                                       isLegalNodeToMoveCheck: isaAvailableNodeToMove)]
        }
        
        return intervals
    }
    
    func isLegalPointToMove(point:GridPoint) -> Bool {
        var result = false
        availableMovesIntervals().forEach { interval in
            if (interval.contains(point)) {
                print("found point at \(interval)")
                result = true
            }
        }
        
        return result
    }
    
    static let allDirections:[CompassPoint] = [.N, .NE, .E, .SE, .S, .SW, .W, .NW]

    // How many squares we can move for adjacent direction provided according to current wind direction
    private func normalWindShiftValueFor(direction:CompassPoint) -> Int {
        let adjacentDirectionIndex = Grid.allDirections.index(of: direction)!
        let currentWindIndex = Grid.allDirections.index(of: dial.windDirection)!
        
        // for normal wind strength
        switch (abs(adjacentDirectionIndex - currentWindIndex)) {
        // Up to 6 squares with the wind (running sails- directly downwind, 180°) (see fig. #... for
        // this and other points in the paragraph)
        // Up to 6 squares to the adjacent sectors (broad reach sails - between running and a beam reach, 135° in our game)
        case 0, 1, 7:
            return 6
        // Up to 4 squares perpendicular to the wind (beam reach sails - 90° to the wind).
        case 2,6:
            return 4
        // Up to 2 squares slantways back (close haul sails - the minimum angle to the wind that the
        // boat and its rig can manage - 45° in the game).
        case 3,5:
            return 2
            
        // You cannot move directly against the wind (The no-go zone)
        default:
            return 0
        }
    }
    
    //Returns point for adjacent direction with shift value provided
    private func shiftedPointWith(direction:CompassPoint, shift:Int) -> GridPoint {
        switch direction {
        case .N:
            return GridPoint(row: Swift.max(0, shipPosition.row - shift), col: shipPosition.col)
        case .NE:
            let rowShift = shipPosition.row - Swift.max(0, shipPosition.row - shift)
            let colShift = Swift.min(shipPosition.col + shift, cols - 1) - shipPosition.col
            let actualShift = Swift.min(rowShift, colShift)
            return GridPoint(row:shipPosition.row - actualShift, col: shipPosition.col + actualShift)
        case .E:
            return GridPoint(row: shipPosition.row, col: Swift.min(shipPosition.col + shift, cols - 1))
        case .SE:
            let rowShift = Swift.min(shipPosition.row + shift, rows - 1) - shipPosition.row
            let colShift = Swift.min(shipPosition.col + shift, cols - 1) - shipPosition.col
            let actualShift = Swift.min(rowShift, colShift)
            return GridPoint(row: shipPosition.row + actualShift, col: shipPosition.col + actualShift)
        case .S:
            return GridPoint(row: Swift.min(shipPosition.row + shift, rows - 1), col: shipPosition.col)
        case .SW:
            let rowShift = Swift.min(shipPosition.row + shift, rows - 1) - shipPosition.row
            let colShift = shipPosition.col - Swift.max(0, shipPosition.col - shift)
            let actualShift = Swift.min(rowShift, colShift)
            return GridPoint(row: shipPosition.row + actualShift, col: shipPosition.col - actualShift)
        case .W:
            return GridPoint(row: shipPosition.row, col: Swift.max(0, shipPosition.col - shift))
        case .NW:
            let rowShift = shipPosition.row - Swift.max(0, shipPosition.row - shift)
            let colShift = shipPosition.col - Swift.max(0, shipPosition.col - shift)
            let actualShift = Swift.min(rowShift, colShift)
            return GridPoint(row: shipPosition.row - actualShift, col: shipPosition.col - actualShift)
        }
    }
    
    //MARK: Land points
    
    func loadLandPointsIfNeeded(with screenshot: UIImage,
                                map: @escaping (CGPoint) -> CGPoint,
                                completion: @escaping () -> Void) {
        if readLandingPointsFromDisk() == nil {
            DispatchQueue.global(qos: .background).async { [weak self] in
                guard let weakSelf = self else {
                    return
                }
                
                for point in weakSelf {
                    
                    let (result, _) = weakSelf.isLandNode(at: point,
                                                          with: screenshot,
                                                          map: map)
                    
                    if result {
                        weakSelf.landPoints.append(point)
                    }
                }
                
                weakSelf.writeLandingPointsToDisk()
                
                DispatchQueue.main.async {
                    completion()
                }
            }
        } else {
            completion()
        }
    }
    
    func isaAvailableNodeToMove(at point:GridPoint) -> Bool {
        return !isLandNode(at: point) && !isNavigationControlNode(at: point)
    }
    
    func isLandNode(at point:GridPoint) -> Bool {
        return landPoints.contains(point)
    }
    
    func isNavigationControlNode(at point:GridPoint) -> Bool {
        if point.col <= 5 && point.row <= 5 {
            return true
        }
        
        return false
    }
    
    func isLandNode(at point:GridPoint,
                    with screenshot:UIImage,
                    map:(CGPoint) -> CGPoint) -> (Bool, [PixelData]) {
        var nodeRect = gridRect(gridPoint: point)
        nodeRect.origin = map(nodeRect.origin)
        
        let seaColorRGBComponents = PixelData(a: 255, r: 86, g: 172, b: 223)
        let seaColorRGBComponents2 = PixelData(a: 255, r: 76, g: 197, b: 219)
        
        let totalNumberOfPoints = Int(nodeRect.width * nodeRect.height)
        var gridPointPixelData: [PixelData] = []
        var seaNumberOfPoints = 0
        
        for x in Int(nodeRect.origin.x)..<Int(nodeRect.origin.x + nodeRect.size.width) {
            for y in Int(nodeRect.origin.y)..<Int(nodeRect.origin.y + nodeRect.size.height) {
                let rgbPixelData = screenshot.rgbComponents(at: CGPoint(x: x, y: y))
                if rgbPixelData == seaColorRGBComponents || rgbPixelData == seaColorRGBComponents2 {
                    seaNumberOfPoints += 1
                }
                
                gridPointPixelData.append(rgbPixelData)
            }
        }
        
        //        print("point: \(point) isLandNode \(seaNumberOfPoints < totalNumberOfPoints/4) with seaNumberOfPoints: \(seaNumberOfPoints)")
        //if half of points is sea then we claim it as a 'sea' point otherwise it is 'land'
        return (seaNumberOfPoints < totalNumberOfPoints/4, gridPointPixelData)
    }
    
    func readLandingPointsFromDisk() -> [GridPoint]? {
        do {
            if let data = try? Data(contentsOf: landingPointsURL) {
                let decoder = PropertyListDecoder()
                landPoints = try decoder.decode([GridPoint].self, from: data)
                return landPoints
            }
        } catch {
            print(error)
        }
        
        return nil
    }
    
    func writeLandingPointsToDisk() {
        let encoder = PropertyListEncoder()
        encoder.outputFormat = .xml
        
        do {
            let data = try encoder.encode(landPoints)
            try data.write(to: landingPointsURL)
        } catch {
            print(error)
        }
    }
}

