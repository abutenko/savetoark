//
//  Dial.swift
//  SaveToArk
//
//  Created by alexbutenko on 12/22/16.
//  Copyright © 2016 alexbutenko. All rights reserved.
//

import Foundation
import CoreGraphics

enum CompassPoint: UInt8 {
    case N, NE, E, SE, S, SW, W, NW
}

enum WindStrength: UInt8 {
    init(rawValue: UInt8) {
        switch rawValue {
        case 0:
            self = .storm
            
        case 1..<Dial.sectorsPerWindDirection-1:
            self = .noWind
            
        case Dial.sectorsPerWindDirection-1:
            self = .normal
            
        default:
            self = .noWind
        }
    }
    
    case storm, noWind, normal
}

class Dial {
    static let sectorsPerWindDirection: UInt8 = 6
    static let windDirectionCount: UInt8 = 8
    static let sectorsCount: UInt8 = windDirectionCount * sectorsPerWindDirection

    var generatedValue: UInt8 = 0
    var windDirection: CompassPoint {
        get {
            return CompassPoint(rawValue: generatedValue/Dial.sectorsPerWindDirection)!
        }
    }
    
    var isPrimarySector: Bool {
        get {
            return ((generatedValue + 1) / Dial.sectorsPerWindDirection) % 2 == 0
        }
    }
    
    var windStrength: WindStrength {
        get {
            return WindStrength(rawValue: generatedValue % Dial.sectorsPerWindDirection)
        }
    }
    
    var onStorm: (()->Void)?
    
    func generateSector() {
        generatedValue = UInt8(arc4random_uniform(UInt32(Dial.sectorsCount)))
        print("dial generated sector => {\(self)}")
        if windStrength == .storm {
            onStorm?()
        }
    }
    
    var angle: CGFloat {
        get {
            let generatedValueWithOffset = Double(generatedValue) - 2.5
            return CGFloat(Double.pi * generatedValueWithOffset / Double(Dial.sectorsCount / 2))
        }
    }
}

extension Dial: CustomStringConvertible {
    var description: String {
        return "wind direction: \(windDirection), wind strength: \(windStrength) value: \(generatedValue)"
    }
}
