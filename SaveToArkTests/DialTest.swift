//
//  DialTest.swift
//  SaveToArk
//
//  Created by alexbutenko on 12/22/16.
//  Copyright © 2016 alexbutenko. All rights reserved.
//

import XCTest
@testable import SaveToArk

class DialTest: XCTestCase {
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInit() {
        let dial = Dial()
        XCTAssertTrue(dial.generatedValue == 0)
        XCTAssertTrue(dial.windDirection == .N)
    }
    
    func testPrimaryWindDirections() {
        let dial = Dial()
        //test first and last
        dial.generatedValue = Dial.sectorsPerWindDirection * 2
        XCTAssertTrue(dial.windDirection == .E)
        
        //skip secondary sector
        dial.generatedValue = Dial.sectorsPerWindDirection * 6
        XCTAssertTrue(dial.windDirection == .W)
    }
    
    func testSecondaryWindDirections() {
        let dial = Dial()
        //test first and last
        dial.generatedValue = Dial.sectorsPerWindDirection
        XCTAssertTrue(dial.windDirection == .NE)
        
        dial.generatedValue = Dial.sectorsPerWindDirection * 7
        XCTAssertTrue(dial.windDirection == .NW)
    }
    
    func testSectorPrimarity() {
        let dial = Dial()
        XCTAssertTrue(dial.isPrimarySector)
        
        dial.generatedValue = Dial.sectorsPerWindDirection
        XCTAssertFalse(dial.isPrimarySector)
        
        dial.generatedValue = Dial.sectorsPerWindDirection * 6
        XCTAssertTrue(dial.isPrimarySector)
        
        dial.generatedValue = Dial.sectorsPerWindDirection * 7
        XCTAssertFalse(dial.isPrimarySector)
    }
    
    func testWindStrength() {
        let dial = Dial()
        XCTAssertTrue(dial.windStrength == .storm)
        
        dial.generatedValue = 1
        XCTAssertTrue(dial.windStrength == .noWind)
        
        dial.generatedValue = Dial.sectorsPerWindDirection - 2
        XCTAssertTrue(dial.windStrength == .noWind)
        
        dial.generatedValue = Dial.sectorsPerWindDirection - 1
        XCTAssertTrue(dial.windStrength == .normal)
        
        dial.generatedValue = Dial.sectorsPerWindDirection
        XCTAssertTrue(dial.windStrength == .storm)
    }
}
