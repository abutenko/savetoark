//
//  GridTest.swift
//  SaveToArk
//
//  Created by alexbutenko on 4/26/17.
//  Copyright © 2017 alexbutenko. All rights reserved.
//

import XCTest
@testable import SaveToArk

class GridTest: XCTestCase {
    var grid:Grid!
    
    override func setUp() {
        super.setUp()
        grid = Grid(blockSize: 64.0, rows: 26, cols: 20, dial: Dial(), shipPosition:GridPoint(row:11, col:11))
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testWithNormalWindStrength() {
        grid.dial.generatedValue = Dial.sectorsPerWindDirection - 1
        XCTAssertTrue(grid.dial.windStrength == .normal)
        XCTAssertTrue(grid.dial.windDirection == .N)

        let availableMoveIntervals = grid.availableMovesIntervals()
        
        //180°, same as current wind direction
        let northInterval = GridInterval(startPoint: grid.shipPosition,
                                                         endPoint: GridPoint(row: grid.shipPosition.row - 6,
                                                                             col: grid.shipPosition.col))
        //135°
        let northWestInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row - 6,
                                                                 col: grid.shipPosition.col - 6))
        let northEastInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row - 6,
                                                                 col: grid.shipPosition.col + 6))
        
        //90°
        let westInterval = GridInterval(startPoint: grid.shipPosition,
                                        endPoint: GridPoint(row: grid.shipPosition.row,
                                                                 col: grid.shipPosition.col - 4))
        let eastInterval = GridInterval(startPoint: grid.shipPosition,
                                        endPoint: GridPoint(row: grid.shipPosition.row,
                                                            col: grid.shipPosition.col + 4))
        
        //45°
        let southWestInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row + 2,
                                                                 col: grid.shipPosition.col - 2))
        let southEastInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row + 2,
                                                                 col: grid.shipPosition.col + 2))
        
        //0°
        let southInterval = GridInterval(startPoint: grid.shipPosition,
                                         endPoint: GridPoint(row: grid.shipPosition.row,
                                                             col: grid.shipPosition.col))
        
        XCTAssertTrue(availableMoveIntervals[0] == northInterval)
        XCTAssertTrue(availableMoveIntervals[1] == northEastInterval)
        XCTAssertTrue(availableMoveIntervals[2] == eastInterval)
        XCTAssertTrue(availableMoveIntervals[3] == southEastInterval)
        XCTAssertTrue(availableMoveIntervals[4] == southInterval)
        XCTAssertTrue(availableMoveIntervals[5] == southWestInterval)
        XCTAssertTrue(availableMoveIntervals[6] == westInterval)
        XCTAssertTrue(availableMoveIntervals[7] == northWestInterval)
    }
    
    func testWithNormalWindStrengthWithShipZeroPosition() {
        grid.dial.generatedValue = Dial.sectorsPerWindDirection - 1
        XCTAssertTrue(grid.dial.windStrength == .normal)
        XCTAssertTrue(grid.dial.windDirection == .N)
        
        grid.shipPosition = GridPoint(row:0, col:0)
        
        let availableMoveIntervals = grid.availableMovesIntervals()
        
        //180°, same as current wind direction
        let northInterval = GridInterval(startPoint: grid.shipPosition,
                                         endPoint: GridPoint(row: grid.shipPosition.row,
                                                             col: grid.shipPosition.col))
        //135°
        let northWestInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row,
                                                                 col: grid.shipPosition.col))
        let northEastInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row,
                                                                 col: grid.shipPosition.col))
        
        //90°
        let westInterval = GridInterval(startPoint: grid.shipPosition,
                                        endPoint: GridPoint(row: grid.shipPosition.row,
                                                            col: grid.shipPosition.col))
        let eastInterval = GridInterval(startPoint: grid.shipPosition,
                                        endPoint: GridPoint(row: grid.shipPosition.row,
                                                            col: grid.shipPosition.col + 4))
        
        //45°
        let southWestInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row,
                                                                 col: grid.shipPosition.col))
        let southEastInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row + 2,
                                                                 col: grid.shipPosition.col + 2))
        
        //0°
        let southInterval = GridInterval(startPoint: grid.shipPosition,
                                         endPoint: GridPoint(row: grid.shipPosition.row,
                                                             col: grid.shipPosition.col))
        
        XCTAssertTrue(availableMoveIntervals[0] == northInterval)
        XCTAssertTrue(availableMoveIntervals[1] == northEastInterval)
        XCTAssertTrue(availableMoveIntervals[2] == eastInterval)
        XCTAssertTrue(availableMoveIntervals[3] == southEastInterval)
        XCTAssertTrue(availableMoveIntervals[4] == southInterval)
        XCTAssertTrue(availableMoveIntervals[5] == southWestInterval)
        XCTAssertTrue(availableMoveIntervals[6] == westInterval)
        XCTAssertTrue(availableMoveIntervals[7] == northWestInterval)
    }
    
    func testWithNormalWindStrengthWithShipFurthermostPosition() {
        grid.dial.generatedValue = Dial.sectorsPerWindDirection - 1
        XCTAssertTrue(grid.dial.windStrength == .normal)
        XCTAssertTrue(grid.dial.windDirection == .N)
        
        grid.shipPosition = GridPoint(row:grid.rows-1, col:grid.cols-1)
        
        let availableMoveIntervals = grid.availableMovesIntervals()
        
        //180°, same as current wind direction
        let northInterval = GridInterval(startPoint: grid.shipPosition,
                                         endPoint: GridPoint(row: grid.shipPosition.row - 6,
                                                             col: grid.shipPosition.col))
        //135°
        let northWestInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row - 6,
                                                                 col: grid.shipPosition.col - 6))
        let northEastInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row,
                                                                 col: grid.shipPosition.col))
        
        //90°
        let westInterval = GridInterval(startPoint: grid.shipPosition,
                                        endPoint: GridPoint(row: grid.shipPosition.row,
                                                            col: grid.shipPosition.col - 4))
        let eastInterval = GridInterval(startPoint: grid.shipPosition,
                                        endPoint: GridPoint(row: grid.shipPosition.row,
                                                            col: grid.shipPosition.col))
        
        //45°
        let southWestInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row,
                                                                 col: grid.shipPosition.col))
        let southEastInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row,
                                                                 col: grid.shipPosition.col))
        
        //0°
        let southInterval = GridInterval(startPoint: grid.shipPosition,
                                         endPoint: GridPoint(row: grid.shipPosition.row,
                                                             col: grid.shipPosition.col))
        
        XCTAssertTrue(availableMoveIntervals[0] == northInterval)
        XCTAssertTrue(availableMoveIntervals[1] == northEastInterval)
        XCTAssertTrue(availableMoveIntervals[2] == eastInterval)
        XCTAssertTrue(availableMoveIntervals[3] == southEastInterval)
        XCTAssertTrue(availableMoveIntervals[4] == southInterval)
        XCTAssertTrue(availableMoveIntervals[5] == southWestInterval)
        XCTAssertTrue(availableMoveIntervals[6] == westInterval)
        XCTAssertTrue(availableMoveIntervals[7] == northWestInterval)
    }
    
    func testWithNormalWindStrengthWithShipZeroPositionWest() {
        grid.dial.generatedValue = Dial.sectorsPerWindDirection - 1 + (Dial.sectorsPerWindDirection * 6)
        XCTAssertTrue(grid.dial.windStrength == .normal)
        XCTAssertTrue(grid.dial.windDirection == .W)
        
        grid.shipPosition = GridPoint(row:11, col:0)
        
        let availableMoveIntervals = grid.availableMovesIntervals()
        
        //180°, same as current wind direction
        let westInterval = GridInterval(startPoint: grid.shipPosition,
                                          endPoint: grid.shipPosition)
        //135°
        let northWestInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: grid.shipPosition)
        let southWestInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: grid.shipPosition)
        
        //90°
        let northInterval = GridInterval(startPoint: grid.shipPosition,
                                        endPoint: GridPoint(row: grid.shipPosition.row - 4,
                                                            col: grid.shipPosition.col))
        let southInterval = GridInterval(startPoint: grid.shipPosition,
                                        endPoint: GridPoint(row: grid.shipPosition.row + 4,
                                                            col: grid.shipPosition.col))
        
        //45°
        let northEastInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row - 2,
                                                                 col: grid.shipPosition.col + 2))
        let southEastInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row + 2,
                                                                 col: grid.shipPosition.col + 2))
        
        //0°
        let eastInterval = GridInterval(startPoint: grid.shipPosition,
                                         endPoint: GridPoint(row: grid.shipPosition.row,
                                                             col: grid.shipPosition.col))
        
        XCTAssertTrue(availableMoveIntervals[0] == northInterval)
        XCTAssertTrue(availableMoveIntervals[1] == northEastInterval)
        XCTAssertTrue(availableMoveIntervals[2] == eastInterval)
        XCTAssertTrue(availableMoveIntervals[3] == southEastInterval)
        XCTAssertTrue(availableMoveIntervals[4] == southInterval)
        XCTAssertTrue(availableMoveIntervals[5] == southWestInterval)
        XCTAssertTrue(availableMoveIntervals[6] == westInterval)
        XCTAssertTrue(availableMoveIntervals[7] == northWestInterval)
    }
    
    func testWithNoWind() {
        grid.dial.generatedValue = WindStrength.noWind.rawValue
        XCTAssertTrue(grid.dial.windStrength == .noWind)
        XCTAssertTrue(grid.dial.windDirection == .N)

        let availableMoveIntervals = grid.availableMovesIntervals()

        //180°, same as current wind direction
        let northInterval = GridInterval(startPoint: grid.shipPosition,
                                         endPoint: GridPoint(row: grid.shipPosition.row - 1,
                                                             col: grid.shipPosition.col))
        //135°
        let northWestInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row - 1,
                                                                 col: grid.shipPosition.col - 1))
        let northEastInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row - 1,
                                                                 col: grid.shipPosition.col + 1))
        
        //90°
        let westInterval = GridInterval(startPoint: grid.shipPosition,
                                        endPoint: GridPoint(row: grid.shipPosition.row,
                                                            col: grid.shipPosition.col - 1))
        let eastInterval = GridInterval(startPoint: grid.shipPosition,
                                        endPoint: GridPoint(row: grid.shipPosition.row,
                                                            col: grid.shipPosition.col + 1))
        
        //45°
        let southWestInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row + 1,
                                                                 col: grid.shipPosition.col - 1))
        let southEastInterval = GridInterval(startPoint: grid.shipPosition,
                                             endPoint: GridPoint(row: grid.shipPosition.row + 1,
                                                                 col: grid.shipPosition.col + 1))
        
        //0°
        let southInterval = GridInterval(startPoint: grid.shipPosition,
                                         endPoint: GridPoint(row: grid.shipPosition.row + 1,
                                                             col: grid.shipPosition.col))
        
        XCTAssertTrue(availableMoveIntervals[0] == northInterval)
        XCTAssertTrue(availableMoveIntervals[1] == northEastInterval)
        XCTAssertTrue(availableMoveIntervals[2] == eastInterval)
        XCTAssertTrue(availableMoveIntervals[3] == southEastInterval)
        XCTAssertTrue(availableMoveIntervals[4] == southInterval)
        XCTAssertTrue(availableMoveIntervals[5] == southWestInterval)
        XCTAssertTrue(availableMoveIntervals[6] == westInterval)
        XCTAssertTrue(availableMoveIntervals[7] == northWestInterval)
    }
    
    func testStorm() {
        grid.dial.generatedValue = WindStrength.storm.rawValue
        XCTAssertTrue(grid.dial.windStrength == .storm)
        XCTAssertTrue(grid.dial.windDirection == .N)

        let availableMoveIntervals = grid.availableMovesIntervals()
        XCTAssertTrue(1 == availableMoveIntervals.count,
                      "unexpected intervals count \(availableMoveIntervals.count)")
    }
}
